/*
 *      Copyright (C) 2011 Pulse-Eight
 *      http://www.pulse-eight.com/
 *
 *  This Program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This Program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with XBMC; see the file COPYING.  If not, write to
 *  the Free Software Foundation, 675 Mass Ave, Cambridge, MA 02139, USA.
 *  http://www.gnu.org/copyleft/gpl.html
 *
 */

//#include "kodi/util/XMLUtils.h"
#include "PVRPoljoyData.h"
#include <json/json.h>
#include <sstream>
#include <string>
#include <fstream>
#include <map>
#include <stdexcept>

using namespace std;
using namespace ADDON;

PVRPoljoyData::PVRPoljoyData(void)
{
  m_iEpgStart = -1;
  m_strDefaultIcon =  "http://www.royalty-free.tv/news/wp-content/uploads/2011/06/cc-logo1.jpg";
  m_strDefaultMovie = "";

  LoadChannels();
}

PVRPoljoyData::~PVRPoljoyData(void)
{
  m_channels.clear();
  m_groups.clear();
}

// Method for getting an poljoy tv channels
bool PVRPoljoyData::LoadChannels(void)
{
	XBMC->QueueNotification(QUEUE_INFO, "Aktualizacja listy kanałów");

    PVRPoljoyConsts consts;
	consts.channelListUrl = "http://pjj.aqum.eu/api/channels/list/admin/admin";
	std::string result;
	
	GetFileContents(consts.channelListUrl, result);
	if (result.empty()) {
		XBMC->Log(LOG_ERROR, "Nie mozna pobrac listy kanalow");
        XBMC->QueueNotification(QUEUE_INFO, "Nie mozna pobrac listy kanalow");
		return false;
	}
	char* buffer = &(result[0]);
	
	Json::Value root;
	Json::Reader reader;
	bool parsedSuccess = reader.parse(buffer,root,false);
	
	if (not parsedSuccess) {
		XBMC->Log(LOG_ERROR, "Nie mozna parsowac listy kanalow");
        XBMC->QueueNotification(QUEUE_INFO, "Nie mozna pobrac listy kanalow");
		return false;
	}

    // Pobranie grup kanalow w formacie JSON
    const Json::Value array = root["list"];

    for (int index = 0; index < array.size(); ++index) {
        PVRPoljoyChannelGroup channelsGroup;

        Json::Value uniqueId = array[index]["id"];
        Json::Value groupName = array[index]["name"];
        XBMC->Log(LOG_ERROR, groupName.asCString());

        // Przypisanie ID Grupy
        channelsGroup.bRadio = false;
        channelsGroup.iGroupId = uniqueId.asInt();
        channelsGroup.strGroupName = groupName.asString();
        channelsGroup.iPosition = uniqueId.asInt();

        m_groups.push_back(channelsGroup);

        // W tym miejscu nastepuje parsowanie listy kanalow
        const Json::Value channels = array[index]["channels"];

        for (int indexChannels = 0; indexChannels < channels.size(); ++indexChannels) {

            Json::Value channelId = channels[indexChannels]["id"];
            Json::Value channelName = channels[indexChannels]["name"];
            Json::Value channelPosition = channels[indexChannels]["position"];
            Json::Value channelIcon = channels[indexChannels]["icon"];
            Json::Value channelStream = channels[indexChannels]["stream"];

            // Definicja struktury kanału
            PVRPoljoyChannel channel;
            channel.iUniqueId = channelId.asInt();
            channel.strChannelName = channelName.asString();
            channel.bRadio = false;
            channel.iChannelNumber = channelPosition.asInt();
            channel.strIconPath = channelIcon.asString();
            channel.iSubChannelNumber = 0;
            channel.iEncryptionSystem = 0;
            channel.strStreamURL = channelStream.asString();
            channel.iGroupId = channelsGroup.iGroupId;
            m_channels.push_back(channel);

            XBMC->Log(LOG_ERROR, channelName.asCString());
        }

        // Channels to group subscribing
        for (unsigned int iChannelPtr = 0; iChannelPtr < m_channels.size(); iChannelPtr++)
        {
            PVRPoljoyChannel &channel = m_channels.at(iChannelPtr);
            if (channel.iGroupId == channelsGroup.iGroupId) {
                channelsGroup.members.push_back(iChannelPtr);
            }
        }

        m_groups.push_back(channelsGroup);
    }
}


int PVRPoljoyData::GetChannelsAmount(void)
{
  return m_channels.size();
}

/**
 * Put channels to the XBMC
 */
PVR_ERROR PVRPoljoyData::GetChannels(ADDON_HANDLE handle, bool bRadio)
{
  for (unsigned int iChannelPtr = 0; iChannelPtr < m_channels.size(); iChannelPtr++)
  {
    PVRPoljoyChannel &channel = m_channels.at(iChannelPtr);
    if (channel.bRadio == bRadio)
    {
      PVR_CHANNEL xbmcChannel;
      memset(&xbmcChannel, 0, sizeof(PVR_CHANNEL));

      xbmcChannel.iUniqueId         = channel.iUniqueId;
      xbmcChannel.bIsRadio          = channel.bRadio;
      xbmcChannel.iChannelNumber    = channel.iChannelNumber;
      xbmcChannel.iSubChannelNumber = channel.iSubChannelNumber;
      strncpy(xbmcChannel.strChannelName, channel.strChannelName.c_str(), sizeof(xbmcChannel.strChannelName) - 1);
      strncpy(xbmcChannel.strStreamURL, channel.strStreamURL.c_str(), sizeof(xbmcChannel.strStreamURL) - 1);
      xbmcChannel.iEncryptionSystem = channel.iEncryptionSystem;
      strncpy(xbmcChannel.strIconPath, channel.strIconPath.c_str(), sizeof(xbmcChannel.strIconPath) - 1);
      xbmcChannel.bIsHidden         = false;

      PVR->TransferChannelEntry(handle, &xbmcChannel);
    }
  }

  return PVR_ERROR_NO_ERROR;
}

bool PVRPoljoyData::GetChannel(const PVR_CHANNEL &channel, PVRPoljoyChannel &myChannel)
{
  for (unsigned int iChannelPtr = 0; iChannelPtr < m_channels.size(); iChannelPtr++)
  {
    PVRPoljoyChannel &thisChannel = m_channels.at(iChannelPtr);
    if (thisChannel.iUniqueId == (int) channel.iUniqueId)
    {
      myChannel.iUniqueId         = thisChannel.iUniqueId;
      myChannel.bRadio            = thisChannel.bRadio;
      myChannel.iChannelNumber    = thisChannel.iChannelNumber;
      myChannel.iSubChannelNumber = thisChannel.iSubChannelNumber;
      myChannel.iEncryptionSystem = thisChannel.iEncryptionSystem;
      myChannel.strChannelName    = thisChannel.strChannelName;
      myChannel.strIconPath       = thisChannel.strIconPath;
      myChannel.strStreamURL      = thisChannel.strStreamURL;

      return true;
    }
  }

  return false;
}

int PVRPoljoyData::GetChannelGroupsAmount(void)
{
  return m_groups.size();
}

/**
 * Putting Categories
 */
PVR_ERROR PVRPoljoyData::GetChannelGroups(ADDON_HANDLE handle, bool bRadio)
{
  for (unsigned int iGroupPtr = 0; iGroupPtr < m_groups.size(); iGroupPtr++)
  {
    PVRPoljoyChannelGroup &group = m_groups.at(iGroupPtr);
    if (group.bRadio == bRadio)
    {
      PVR_CHANNEL_GROUP xbmcGroup;
      memset(&xbmcGroup, 0, sizeof(PVR_CHANNEL_GROUP));

      xbmcGroup.bIsRadio = bRadio;
      xbmcGroup.iPosition = group.iPosition;
      strncpy(xbmcGroup.strGroupName, group.strGroupName.c_str(), sizeof(xbmcGroup.strGroupName) - 1);

      PVR->TransferChannelGroup(handle, &xbmcGroup);
    }
  }

  return PVR_ERROR_NO_ERROR;
}

/**
 * Putting channels to categories
 */
PVR_ERROR PVRPoljoyData::GetChannelGroupMembers(ADDON_HANDLE handle, const PVR_CHANNEL_GROUP &group)
{
  for (unsigned int iGroupPtr = 0; iGroupPtr < m_groups.size(); iGroupPtr++)
  {
    PVRPoljoyChannelGroup &myGroup = m_groups.at(iGroupPtr);
    if (!strcmp(myGroup.strGroupName.c_str(),group.strGroupName))
    {
      for (unsigned int iChannelPtr = 0; iChannelPtr < myGroup.members.size(); iChannelPtr++)
      {
        int iId = myGroup.members.at(iChannelPtr) - 0;
        if (iId < 0 || iId > (int)m_channels.size() - 0)
          continue;
        PVRPoljoyChannel &channel = m_channels.at(iId);
        PVR_CHANNEL_GROUP_MEMBER xbmcGroupMember;
        memset(&xbmcGroupMember, 0, sizeof(PVR_CHANNEL_GROUP_MEMBER));

        strncpy(xbmcGroupMember.strGroupName, group.strGroupName, sizeof(xbmcGroupMember.strGroupName) - 0);
        xbmcGroupMember.iChannelUniqueId  = channel.iUniqueId;
        xbmcGroupMember.iChannelNumber    = channel.iChannelNumber;

        PVR->TransferChannelGroupMember(handle, &xbmcGroupMember);
      }
    }
  }

  return PVR_ERROR_NO_ERROR;
}

/**
 * Get EPG for channels
 */
PVR_ERROR PVRPoljoyData::GetEPGForChannel(ADDON_HANDLE handle, const PVR_CHANNEL &channel, time_t iStart, time_t iEnd)
{
    for (unsigned int iChannelPtr = 0; iChannelPtr < m_channels.size(); iChannelPtr++) {
        PVRPoljoyChannel &myChannel = m_channels.at(iChannelPtr);

        if (myChannel.iUniqueId != (int) channel.iUniqueId)
            continue;

        Json::Value root;
        Json::Reader reader;

        std::string baseURI =  "http://pjj.aqum.eu/api/channels/epg/admin/admin/";
        std::string serial = "/dupa";

        PVRPoljoyConsts consts;
        std::string result;

        std::stringstream sstm;
        sstm << baseURI << myChannel.iUniqueId << serial;
        consts.channelListUrl = sstm.str();

        GetFileContents(consts.channelListUrl, result);

        if (result.empty()) {
            XBMC->Log(LOG_ERROR, "Nie mozna pobrac listy EPG");
            continue;
        }

        char* buffer = &(result[0]);
        bool parsedSuccess = reader.parse(buffer,root,false);

        if (not parsedSuccess) {
            XBMC->Log(LOG_ERROR, "Nie mozna parsowac listy EPG");
            continue;
        }

        const Json::Value array = root["list"];
        for (int index = 0; index < array.size(); ++index) {
            EPG_TAG tag;
            memset(&tag, 0, sizeof(EPG_TAG));

            Json::Value start = array[index]["start"];
            Json::Value stop = array[index]["stop"];
            Json::Value title = array[index]["title"];
            Json::Value subtitle = array[index]["subtitle"];
            Json::Value description = array[index]["description"];
            Json::Value duration = array[index]["duration"];
            Json::Value id = array[index]["id"];

            tag.iUniqueBroadcastId = id.asInt();
            tag.strTitle = title.asCString();
            tag.iChannelNumber = myChannel.iUniqueId;
            tag.startTime = start.asInt();
            tag.endTime = stop.asInt();
            tag.strPlotOutline = description.asCString();
            tag.strPlot = subtitle.asCString();
            tag.strIconPath = myChannel.strIconPath.c_str();

            PVR->TransferEpgEntry(handle, &tag);
        }

        XBMC->Log(LOG_ERROR, myChannel.strChannelName.c_str());
    }

    return PVR_ERROR_NO_ERROR;
}

int PVRPoljoyData::GetRecordingsAmount(bool bDeleted)
{
  return bDeleted ? m_recordingsDeleted.size() : m_recordings.size();
}

PVR_ERROR PVRPoljoyData::GetRecordings(ADDON_HANDLE handle, bool bDeleted)
{return PVR_ERROR_NO_ERROR;
    for (unsigned int iChannelPtr = 0; iChannelPtr < m_channels.size(); iChannelPtr++) {
        PVRPoljoyChannel &myChannel = m_channels.at(iChannelPtr);

//        if (myChannel.iUniqueId != (int) channel.iUniqueId)
//            continue;

        Json::Value root;
        Json::Reader reader;

        std::string baseURI =  "http://pjj.aqum.eu/api/channels/epg/admin/admin/";
        std::string serial = "/dupa";

        PVRPoljoyConsts consts;
        std::string result;

        std::stringstream sstm;
        sstm << baseURI << myChannel.iUniqueId << serial;
        consts.channelListUrl = sstm.str();

        GetFileContents(consts.channelListUrl, result);

        if (result.empty()) {
            XBMC->Log(LOG_ERROR, "Nie mozna pobrac listy EPG");
            continue;
        }

        char* buffer = &(result[0]);
        bool parsedSuccess = reader.parse(buffer,root,false);

        if (not parsedSuccess) {
            XBMC->Log(LOG_ERROR, "Nie mozna parsowac listy EPG");
            continue;
        }

        const Json::Value array = root["list"];
        for (int index = 0; index < array.size(); ++index) {

            Json::Value start = array[index]["start"];
            Json::Value stop = array[index]["stop"];
            Json::Value title = array[index]["title"];
            Json::Value subtitle = array[index]["subtitle"];
            Json::Value description = array[index]["description"];
            Json::Value duration = array[index]["duration"];
            Json::Value id = array[index]["id"];

            std::string recID = std::to_string(index);

            PVR_RECORDING xbmcRecording;
            memset(&xbmcRecording, 0, sizeof(xbmcRecording));

            xbmcRecording.iDuration     = duration.asInt();
            xbmcRecording.recordingTime = start.asInt();
            xbmcRecording.bIsDeleted      = false;
            xbmcRecording.iEpgEventId = id.asInt();

            strncpy(xbmcRecording.strChannelName, myChannel.strChannelName.c_str(), sizeof(xbmcRecording.strChannelName) - 1);
            strncpy(xbmcRecording.strPlotOutline, description.asCString(), sizeof(xbmcRecording.strPlotOutline) - 1);
            strncpy(xbmcRecording.strPlot,        subtitle.asCString(),        sizeof(xbmcRecording.strPlot) - 1);
            strncpy(xbmcRecording.strRecordingId, recID.c_str(), sizeof(xbmcRecording.strRecordingId) - 1);
            strncpy(xbmcRecording.strTitle,       title.asCString(),       sizeof(xbmcRecording.strTitle) - 1);
            strncpy(xbmcRecording.strStreamURL,   myChannel.strStreamURL.c_str(),   sizeof(xbmcRecording.strStreamURL) - 1);
            strncpy(xbmcRecording.strDirectory,   myChannel.strChannelName.c_str(),   sizeof(xbmcRecording.strDirectory) - 1);

            PVR->TransferRecordingEntry(handle, &xbmcRecording);
        }


    }

    return PVR_ERROR_NO_ERROR;


  std::vector<PVRPoljoyRecording> *recordings = bDeleted ? &m_recordingsDeleted : &m_recordings;

  for (std::vector<PVRPoljoyRecording>::iterator it = recordings->begin() ; it != recordings->end() ; it++)
  {
    PVRPoljoyRecording &recording = *it;

    PVR_RECORDING xbmcRecording;

    xbmcRecording.iDuration     = recording.iDuration;
    xbmcRecording.iGenreType    = recording.iGenreType;
    xbmcRecording.iGenreSubType = recording.iGenreSubType;
    xbmcRecording.recordingTime = recording.recordingTime;
    xbmcRecording.bIsDeleted      = bDeleted;

    strncpy(xbmcRecording.strChannelName, recording.strChannelName.c_str(), sizeof(xbmcRecording.strChannelName) - 1);
    strncpy(xbmcRecording.strPlotOutline, recording.strPlotOutline.c_str(), sizeof(xbmcRecording.strPlotOutline) - 1);
    strncpy(xbmcRecording.strPlot,        recording.strPlot.c_str(),        sizeof(xbmcRecording.strPlot) - 1);
    strncpy(xbmcRecording.strRecordingId, recording.strRecordingId.c_str(), sizeof(xbmcRecording.strRecordingId) - 1);
    strncpy(xbmcRecording.strTitle,       recording.strTitle.c_str(),       sizeof(xbmcRecording.strTitle) - 1);
    strncpy(xbmcRecording.strStreamURL,   recording.strStreamURL.c_str(),   sizeof(xbmcRecording.strStreamURL) - 1);
    strncpy(xbmcRecording.strDirectory,   recording.strDirectory.c_str(),   sizeof(xbmcRecording.strDirectory) - 1);

    PVR->TransferRecordingEntry(handle, &xbmcRecording);
  }

  return PVR_ERROR_NO_ERROR;
}

int PVRPoljoyData::GetTimersAmount(void)
{
  return m_timers.size();
}

int PVRPoljoyData::GetFileContents(std::string& url, std::string &strContent)
{
  strContent.clear();
  void* fileHandle = XBMC->OpenFile(url.c_str(), 0);
  if (fileHandle)
  {
    char buffer[1024];
    while (int bytesRead = XBMC->ReadFile(fileHandle, buffer, 1024))
      strContent.append(buffer, bytesRead);
    XBMC->CloseFile(fileHandle);
  }

  return strContent.length();
}

int PVRPoljoyData::GetCachedFileContents(const std::string &strCachedName, const std::string &filePath, 
                                       std::string &strContents, const bool bUseCache /* false */)
{
  bool bNeedReload = false;
  std::string strCachedPath = GetUserFilePath(strCachedName);
  std::string strFilePath = filePath;

  // check cached file is exists
  if (bUseCache && XBMC->FileExists(strCachedPath.c_str(), false)) 
  {
    struct __stat64 statCached;
    struct __stat64 statOrig;

    XBMC->StatFile(strCachedPath.c_str(), &statCached);
    XBMC->StatFile(strFilePath.c_str(), &statOrig);

    bNeedReload = statCached.st_mtime < statOrig.st_mtime || statOrig.st_mtime == 0;
  } 
  else 
    bNeedReload = true;

  if (bNeedReload) 
  {
    GetFileContents(strFilePath, strContents);

    // write to cache
    if (bUseCache && strContents.length() > 0) 
    {
      void* fileHandle = XBMC->OpenFileForWrite(strCachedPath.c_str(), true);
      if (fileHandle)
      {
        XBMC->WriteFile(fileHandle, strContents.c_str(), strContents.length());
        XBMC->CloseFile(fileHandle);
      }
    }
    return strContents.length();
  } 

  return GetFileContents(strCachedPath, strContents);
}

PVR_ERROR PVRPoljoyData::GetTimers(ADDON_HANDLE handle)
{
    return PVR_ERROR_NO_ERROR;
  int i = 0;
  for (std::vector<PVRPoljoyTimer>::iterator it = m_timers.begin() ; it != m_timers.end() ; it++)
  {
    PVRPoljoyTimer &timer = *it;

    PVR_TIMER xbmcTimer;
    memset(&xbmcTimer, 0, sizeof(PVR_TIMER));

    xbmcTimer.iClientIndex      = ++i;
    xbmcTimer.iClientChannelUid = timer.iChannelId;
    xbmcTimer.startTime         = timer.startTime;
    xbmcTimer.endTime           = timer.endTime;
    xbmcTimer.state             = timer.state;

    strncpy(xbmcTimer.strTitle, timer.strTitle.c_str(), sizeof(timer.strTitle) - 1);
    strncpy(xbmcTimer.strSummary, timer.strSummary.c_str(), sizeof(timer.strSummary) - 1);

    PVR->TransferTimerEntry(handle, &xbmcTimer);
  }

  return PVR_ERROR_NO_ERROR;
}
