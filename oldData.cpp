//
//
//
//bool PVRPoljoyData::LoadDemoData(void)
//{
//    TiXmlDocument xmlDoc;
//    string strSettingsFile = GetSettingsFile();
//
//    if (!xmlDoc.LoadFile(strSettingsFile))
//    {
//        XBMC->Log(LOG_ERROR, "invalid demo data (no/invalid data file found at '%s')", strSettingsFile.c_str());
//        return false;
//    }
//
//    TiXmlElement *pRootElement = xmlDoc.RootElement();
//    if (strcmp(pRootElement->Value(), "demo") != 0)
//    {
//        XBMC->Log(LOG_ERROR, "invalid demo data (no <demo> tag found)");
//        return false;
//    }
//
//    /* load channels */
//    int iUniqueChannelId = 0;
//    TiXmlElement *pElement = pRootElement->FirstChildElement("channels");
//    if (pElement)
//    {
//        TiXmlNode *pChannelNode = NULL;
//        while ((pChannelNode = pElement->IterateChildren(pChannelNode)) != NULL)
//        {
//            CStdString strTmp;
//            PVRPoljoyChannel channel;
//            channel.iUniqueId = ++iUniqueChannelId;
//
//            /* channel name */
//            if (!XMLUtils::GetString(pChannelNode, "name", strTmp))
//                continue;
//            channel.strChannelName = strTmp;
//
//            /* radio/TV */
//            XMLUtils::GetBoolean(pChannelNode, "radio", channel.bRadio);
//
//            /* channel number */
//            if (!XMLUtils::GetInt(pChannelNode, "number", channel.iChannelNumber))
//                channel.iChannelNumber = iUniqueChannelId;
//
//            /* sub channel number */
//            if (!XMLUtils::GetInt(pChannelNode, "subnumber", channel.iSubChannelNumber))
//                channel.iSubChannelNumber = 0;
//
//            /* CAID */
//            if (!XMLUtils::GetInt(pChannelNode, "encryption", channel.iEncryptionSystem))
//                channel.iEncryptionSystem = 0;
//
//            /* icon path */
//            if (!XMLUtils::GetString(pChannelNode, "icon", strTmp))
//                channel.strIconPath = m_strDefaultIcon;
//            else
//                channel.strIconPath = strTmp;
//
//            /* stream url */
//            if (!XMLUtils::GetString(pChannelNode, "stream", strTmp))
//                channel.strStreamURL = m_strDefaultMovie;
//            else
//                channel.strStreamURL = strTmp;
//
//            m_channels.push_back(channel);
//        }
//    }
//
//    /* load channel groups */
//    int iUniqueGroupId = 0;
//    pElement = pRootElement->FirstChildElement("channelgroups");
//    if (pElement)
//    {
//        TiXmlNode *pGroupNode = NULL;
//        while ((pGroupNode = pElement->IterateChildren(pGroupNode)) != NULL)
//        {
//            CStdString strTmp;
//            PVRPoljoyChannelGroup group;
//            group.iGroupId = ++iUniqueGroupId;
//
//            /* group name */
//            if (!XMLUtils::GetString(pGroupNode, "name", strTmp))
//                continue;
//            group.strGroupName = strTmp;
//
//            /* radio/TV */
//            XMLUtils::GetBoolean(pGroupNode, "radio", group.bRadio);
//
//            /* sort position */
//            XMLUtils::GetInt(pGroupNode, "position", group.iPosition);
//
//            /* members */
//            TiXmlNode* pMembers = pGroupNode->FirstChild("members");
//            TiXmlNode *pMemberNode = NULL;
//            while (pMembers != NULL && (pMemberNode = pMembers->IterateChildren(pMemberNode)) != NULL)
//            {
//                int iChannelId = atoi(pMemberNode->FirstChild()->Value());
//                if (iChannelId > -1)
//                    group.members.push_back(iChannelId);
//            }
//
//            m_groups.push_back(group);
//        }
//    }
//
//    /* load EPG entries */
//    pElement = pRootElement->FirstChildElement("epg");
//    if (pElement)
//    {
//        TiXmlNode *pEpgNode = NULL;
//        while ((pEpgNode = pElement->IterateChildren(pEpgNode)) != NULL)
//        {
//            CStdString strTmp;
//            int iTmp;
//            PVRPoljoyEpgEntry entry;
//
//            /* broadcast id */
//            if (!XMLUtils::GetInt(pEpgNode, "broadcastid", entry.iBroadcastId))
//                continue;
//
//            /* channel id */
//            if (!XMLUtils::GetInt(pEpgNode, "channelid", iTmp))
//                continue;
//            PVRPoljoyChannel &channel = m_channels.at(iTmp - 1);
//            entry.iChannelId = channel.iUniqueId;
//
//            /* title */
//            if (!XMLUtils::GetString(pEpgNode, "title", strTmp))
//                continue;
//            entry.strTitle = strTmp;
//
//            /* start */
//            if (!XMLUtils::GetInt(pEpgNode, "start", iTmp))
//                continue;
//            entry.startTime = iTmp;
//
//            /* end */
//            if (!XMLUtils::GetInt(pEpgNode, "end", iTmp))
//                continue;
//            entry.endTime = iTmp;
//
//            /* plot */
//            if (XMLUtils::GetString(pEpgNode, "plot", strTmp))
//                entry.strPlot = strTmp;
//
//            /* plot outline */
//            if (XMLUtils::GetString(pEpgNode, "plotoutline", strTmp))
//                entry.strPlotOutline = strTmp;
//
//            /* icon path */
//            if (XMLUtils::GetString(pEpgNode, "icon", strTmp))
//                entry.strIconPath = strTmp;
//
//            /* genre type */
//            XMLUtils::GetInt(pEpgNode, "genretype", entry.iGenreType);
//
//            /* genre subtype */
//            XMLUtils::GetInt(pEpgNode, "genresubtype", entry.iGenreSubType);
//
//            XBMC->Log(LOG_DEBUG, "loaded EPG entry '%s' channel '%d' start '%d' end '%d'", entry.strTitle.c_str(), entry.iChannelId, entry.startTime, entry.endTime);
//            channel.epg.push_back(entry);
//        }
//    }
//
//    /* load recordings */
//    iUniqueGroupId = 0; // reset unique ids
//    pElement = pRootElement->FirstChildElement("recordings");
//    if (pElement)
//    {
//        TiXmlNode *pRecordingNode = NULL;
//        while ((pRecordingNode = pElement->IterateChildren(pRecordingNode)) != NULL)
//        {
//            CStdString strTmp;
//            PVRPoljoyRecording recording;
//
//            /* recording title */
//            if (!XMLUtils::GetString(pRecordingNode, "title", strTmp))
//                continue;
//            recording.strTitle = strTmp;
//
//            /* recording url */
//            if (!XMLUtils::GetString(pRecordingNode, "url", strTmp))
//                recording.strStreamURL = m_strDefaultMovie;
//            else
//                recording.strStreamURL = strTmp;
//
//            /* recording path */
//            if (XMLUtils::GetString(pRecordingNode, "directory", strTmp))
//                recording.strDirectory = strTmp;
//
//            iUniqueGroupId++;
//            strTmp.Format("%d", iUniqueGroupId);
//            recording.strRecordingId = strTmp;
//
//            /* channel name */
//            if (XMLUtils::GetString(pRecordingNode, "channelname", strTmp))
//                recording.strChannelName = strTmp;
//
//            /* plot */
//            if (XMLUtils::GetString(pRecordingNode, "plot", strTmp))
//                recording.strPlot = strTmp;
//
//            /* plot outline */
//            if (XMLUtils::GetString(pRecordingNode, "plotoutline", strTmp))
//                recording.strPlotOutline = strTmp;
//
//            /* genre type */
//            XMLUtils::GetInt(pRecordingNode, "genretype", recording.iGenreType);
//
//            /* genre subtype */
//            XMLUtils::GetInt(pRecordingNode, "genresubtype", recording.iGenreSubType);
//
//            /* duration */
//            XMLUtils::GetInt(pRecordingNode, "duration", recording.iDuration);
//
//            /* recording time */
//            if (XMLUtils::GetString(pRecordingNode, "time", strTmp))
//            {
//                time_t timeNow = time(NULL);
//                struct tm* now = localtime(&timeNow);
//
//                CStdString::size_type delim = strTmp.Find(':');
//                if (delim != CStdString::npos)
//                {
//                    now->tm_hour = (int)strtol(strTmp.Left(delim), NULL, 0);
//                    now->tm_min  = (int)strtol(strTmp.Mid(delim + 1), NULL, 0);
//                    now->tm_mday--; // yesterday
//
//                    recording.recordingTime = mktime(now);
//                }
//            }
//
//            m_recordings.push_back(recording);
//        }
//    }
//
//    /* load deleted recordings */
//    pElement = pRootElement->FirstChildElement("recordingsdeleted");
//    if (pElement)
//    {
//        TiXmlNode *pRecordingNode = NULL;
//        while ((pRecordingNode = pElement->IterateChildren(pRecordingNode)) != NULL)
//        {
//            CStdString strTmp;
//            PVRPoljoyRecording recording;
//
//            /* recording title */
//            if (!XMLUtils::GetString(pRecordingNode, "title", strTmp))
//                continue;
//            recording.strTitle = strTmp;
//
//            /* recording url */
//            if (!XMLUtils::GetString(pRecordingNode, "url", strTmp))
//                recording.strStreamURL = m_strDefaultMovie;
//            else
//                recording.strStreamURL = strTmp;
//
//            /* recording path */
//            if (XMLUtils::GetString(pRecordingNode, "directory", strTmp))
//                recording.strDirectory = strTmp;
//
//            iUniqueGroupId++;
//            strTmp.Format("%d", iUniqueGroupId);
//            recording.strRecordingId = strTmp;
//
//            /* channel name */
//            if (XMLUtils::GetString(pRecordingNode, "channelname", strTmp))
//                recording.strChannelName = strTmp;
//
//            /* plot */
//            if (XMLUtils::GetString(pRecordingNode, "plot", strTmp))
//                recording.strPlot = strTmp;
//
//            /* plot outline */
//            if (XMLUtils::GetString(pRecordingNode, "plotoutline", strTmp))
//                recording.strPlotOutline = strTmp;
//
//            /* genre type */
//            XMLUtils::GetInt(pRecordingNode, "genretype", recording.iGenreType);
//
//            /* genre subtype */
//            XMLUtils::GetInt(pRecordingNode, "genresubtype", recording.iGenreSubType);
//
//            /* duration */
//            XMLUtils::GetInt(pRecordingNode, "duration", recording.iDuration);
//
//            /* recording time */
//            if (XMLUtils::GetString(pRecordingNode, "time", strTmp))
//            {
//                time_t timeNow = time(NULL);
//                struct tm* now = localtime(&timeNow);
//
//                CStdString::size_type delim = strTmp.Find(':');
//                if (delim != CStdString::npos)
//                {
//                    now->tm_hour = (int)strtol(strTmp.Left(delim), NULL, 0);
//                    now->tm_min  = (int)strtol(strTmp.Mid(delim + 1), NULL, 0);
//                    now->tm_mday--; // yesterday
//
//                    recording.recordingTime = mktime(now);
//                }
//            }
//
//            m_recordingsDeleted.push_back(recording);
//        }
//    }
//
//    /* load timers */
//    pElement = pRootElement->FirstChildElement("timers");
//    if (pElement)
//    {
//        TiXmlNode *pTimerNode = NULL;
//        while ((pTimerNode = pElement->IterateChildren(pTimerNode)) != NULL)
//        {
//            CStdString strTmp;
//            int iTmp;
//            PVRPoljoyTimer timer;
//            time_t timeNow = time(NULL);
//            struct tm* now = localtime(&timeNow);
//
//            /* channel id */
//            if (!XMLUtils::GetInt(pTimerNode, "channelid", iTmp))
//                continue;
//            PVRPoljoyChannel &channel = m_channels.at(iTmp - 1);
//            timer.iChannelId = channel.iUniqueId;
//
//            /* state */
//            if (XMLUtils::GetInt(pTimerNode, "state", iTmp))
//                timer.state = (PVR_TIMER_STATE) iTmp;
//
//            /* title */
//            if (!XMLUtils::GetString(pTimerNode, "title", strTmp))
//                continue;
//            timer.strTitle = strTmp;
//
//            /* summary */
//            if (!XMLUtils::GetString(pTimerNode, "summary", strTmp))
//                continue;
//            timer.strSummary = strTmp;
//
//            /* start time */
//            if (XMLUtils::GetString(pTimerNode, "starttime", strTmp))
//            {
//                CStdString::size_type delim = strTmp.Find(':');
//                if (delim != CStdString::npos)
//                {
//                    now->tm_hour = (int)strtol(strTmp.Left(delim), NULL, 0);
//                    now->tm_min  = (int)strtol(strTmp.Mid(delim + 1), NULL, 0);
//
//                    timer.startTime = mktime(now);
//                }
//            }
//
//            /* end time */
//            if (XMLUtils::GetString(pTimerNode, "endtime", strTmp))
//            {
//                CStdString::size_type delim = strTmp.Find(':');
//                if (delim != CStdString::npos)
//                {
//                    now->tm_hour = (int)strtol(strTmp.Left(delim), NULL, 0);
//                    now->tm_min  = (int)strtol(strTmp.Mid(delim + 1), NULL, 0);
//
//                    timer.endTime = mktime(now);
//                }
//            }
//
//            XBMC->Log(LOG_DEBUG, "loaded timer '%s' channel '%d' start '%d' end '%d'", timer.strTitle.c_str(), timer.iChannelId, timer.startTime, timer.endTime);
//            m_timers.push_back(timer);
//        }
//    }
//
//    return true;
//}
//
////std::string PVRPoljoyData::GetSettingsFile() const
////{
////  string settingFile = g_strClientPath;
////  if (settingFile.at(settingFile.size() - 1) == '\\' ||
////      settingFile.at(settingFile.size() - 1) == '/')
////    settingFile.append("PVRDemoAddonSettings.xml");
////  else
////    settingFile.append("/PVRDemoAddonSettings.xml");
////  return settingFile;
////}